# BOA FMC+

## Cloning the repository
- Libraries used in the project are in this other [cern repository](https://gitlab.cern.ch/p2-xware/hardware/kicad-pcbs/kit-kicad-library) or [kit repository](https://git.scc.kit.edu/cms-tt/hardware/kicad-pcbs/kit-kicad-library) and need to be linked using an **Environment Variable** inside KiCad (Preferences -> Configure Paths... ->) right after opening the project
 
  `KICAD_USER_LIBRARY = /path/to/KIT_KiCad_Library`

## 3D Views

### Top

![Top View 3D](/images/Top_3D.png)

![Bottom View 3D](/images/Bottom_3D.png)





